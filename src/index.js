import React from 'react';
import ReactDom from 'react-dom';
import App from './components/AppComponent/App';
import './global.css';

ReactDom.render(<App />, document.getElementById('app'));
