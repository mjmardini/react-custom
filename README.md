# What is this?
This is a custom react boilerplate without any extra dependencies.
# Why should I use this?
The use-case for this project depends on what you need. If you want a quick react project without all the testing libraries, or you want to build a prototype without all the fluf then this is the project for you.
# How do I use this?
1. Clone the repository.
2. Run npm install
3. Run npm start for dev server.
    - Run npm build for production.
# Does this work?
Yes, check out [react-custom](http://mjmardini.gitlab.io/react-custom)